/*
 * Euclid Language Skeleton Java program
 * You may use these .java files as a start for your coursework
 * All other coding should be your own (individual coursework)
 */
package euclid;

import java.io.IOException;
import java.util.Hashtable;

public class Euclid {
	public static final int MAX_NUM_TOKENS = 3000; // You may assume there will be no more than 3000 tokens

	public static Token[] tokenSequence = new Token[MAX_NUM_TOKENS];
	public static int currentToken = 0;
	public static Hashtable idents;
	public static boolean lexAnalysisSuccessful = true;

	public static void main(String[] args) {
		LexAnalyser lex = new LexAnalyser();
		Token nextToken = new Token(TokenType.END_OF_LINE);

		System.out.println("Enter your code below:");
		do {
			try {
				nextToken = lex.scan();
				if (nextToken.returnType().toString() != "COMMENT") {
					tokenSequence[currentToken] = nextToken;
					if (nextToken.returnType() == TokenType.NULL_TOKEN) {
						lexAnalysisSuccessful = false;
					}

					currentToken++;
				}

			} catch (IOException ex) {
				System.out.println("--Error in lexical analysis of program. \n");
			}
		} while (nextToken.returnType() != TokenType.END_OF_FILE && lexAnalysisSuccessful == true);

		if(!lexAnalysisSuccessful) {
			return;
		}
		
		for (int a = 0; a < tokenSequence.length; a++) {
			if (tokenSequence[a] == null) {
				break;
			}

			System.out.print(tokenSequence[a].returnType() + " ");

			// Formatting: IF END_OF_LINE then create a new line
			if (tokenSequence[a].returnType().toString().equals("END_OF_LINE")) {
				System.out.println("");
			}
		}

		System.out.println(" ");

		if (lexAnalysisSuccessful) {
			// Lexical analysis complete, now on to parsing..
			System.out.println("--- Beginning Parsing ---");
			idents = lex.getIdentifiers();
			// This next declaration passes the sequence of tokens and hash table of
			// identifiers to the parser
			Parser pars = new Parser(tokenSequence, idents);
			pars.prog();
			System.out.println("--- Ending Parsing ---");

			System.out.println("�Lexical analysis successful.\n");
		}
	}
}
