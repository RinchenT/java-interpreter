
package euclid;

public class KeywordToken extends Token {
    public String keyword;
    
    public KeywordToken(String keywordVal) {
        super(TokenType.KEYWORD);
        keyword = keywordVal;
    }
    
    public String getKeyword() {
        return keyword;
    }
    
    public void print() {
        System.out.println("Keyword Token: " + keyword);
    }
}
