
package euclid;

import java.io.IOException;
import java.util.Hashtable;

public class LexAnalyser {
	public int lineNumber = 1;
	private char peek = ' ';
	private Hashtable<String, IdentToken> identifiers = new Hashtable<String, IdentToken>();
	private Hashtable<String, KeywordToken> reservedKeywords = new Hashtable<String, KeywordToken>();

	void addIdentifier(IdentToken t) {
		identifiers.put(t.identifierName, t);
	}

	public LexAnalyser() {
		// When a LexAnalyser object is created, populate the reservedKeywords
		// hashtable with all keywords
		reservedKeywords.put("WHILE", new KeywordToken("WHILE"));
		reservedKeywords.put("IF", new KeywordToken("IF"));
		reservedKeywords.put("THEN", new KeywordToken("THEN"));
		reservedKeywords.put("ENDIF", new KeywordToken("ENDIF"));
		reservedKeywords.put("ENDWHILE", new KeywordToken("ENDWHILE"));
		reservedKeywords.put("INT", new KeywordToken("INT"));
		reservedKeywords.put("PRINT", new KeywordToken("PRINT"));
		reservedKeywords.put("IS", new KeywordToken("IS"));
		reservedKeywords.put("OUTPUT", new KeywordToken("OUTPUT"));
	}

	// This next method is for you to write, to scan the input and return the
	// correct Token for the next lexeme
	// The comments inside the scan() method may help you to structure your code
	public Token scan() throws IOException {
		for (;; peek = (char) System.in.read()) {
			if (peek == ' ' || peek == '\t' || peek == '\n' || peek == '\r')
				continue;
			else
				break;
		}
		
		// Checks for each character and assigns it a token type
		switch (peek) {
		case ('$'):
			peek = ' ';
			return new Token(TokenType.END_OF_FILE);
		case (';'):
			peek = ' ';
			lineNumber++;
			return new Token(TokenType.END_OF_LINE);
		case ('+'):
			peek = ' ';
			return new OperatorToken(OperatorType.PLUS);
		case ('-'):
			peek = ' ';
			return new OperatorToken(OperatorType.MINUS);
		case ('*'):
			peek = ' ';
			return new OperatorToken(OperatorType.MULTIPLY);
		case ('('):
			peek = ' ';
			return new Token(TokenType.LBRACKET);
		case (')'):
			peek = ' ';
			return new Token(TokenType.RBRACKET);
		case ('>'):
			peek = ' ';
			return new OperatorToken(OperatorType.GREATER_THAN);
		case ('<'):
			peek = ' ';
			return new OperatorToken(OperatorType.LESS_THAN);
		case ('!'):
			peek = ' ';
			return new OperatorToken(OperatorType.NOT_EQUAL);
		case ('%'):
			peek = ' ';
			return new OperatorToken(OperatorType.MODULUS);
		case ('/'): // If comment is started, remove all characters until whitespace or */
			return checkIfComment();
		}
	
		
		// Checks to see if digit has been entered
		if (Character.isDigit(peek)) {
			int digit = 0; 
			do {
				digit = 10 * digit + Character.digit(peek, 10);
				peek = (char) System.in.read();
			} while (Character.isDigit(peek));
			return new NumToken(digit); // Adds the digit to NumToken
		}

		// Checks to see if letter has been entered
		if (Character.isLetter(peek)) {
			StringBuffer buff = new StringBuffer();
			do {
				buff.append(peek);
				peek = (char) System.in.read();
			} while (Character.isLetterOrDigit(peek));
			String buffString = buff.toString();

			if (buffString.equals("INT")) {			
				return new KeywordToken("INT");
			} else if (buffString.equals("WHILE")) {
				return new KeywordToken("WHILE");
			} else if (buffString.equals("THEN")) {
				return new KeywordToken("THEN");
			} else if (buffString.equals("IF")) {
				return new KeywordToken("IF");
			} else if (buffString.equals("ENDIF")) {
				return new KeywordToken("ENDIF");
			} else if (buffString.equals("OUTPUT")) {
				return new KeywordToken("OUTPUT");
			} else if (buffString.equals("ENDWHILE")) {
				return  new KeywordToken("ENDWHILE");
			} else if (buffString.equals("IS")) {
				return new Token(TokenType.EQUALS);
			}

			else {
				// If the code gets to this stage, a word has been entered which
				// is deemed to be an ID - A check is made to see if it is 
				// lowercase, otherwise, return an error
				if(isIdUpperCase(buffString)) {
					System.out.println("ID must be lowercase! This error was made on line " + lineNumber + ".");
					return new Token(TokenType.NULL_TOKEN);
				}else {
					IdentToken newIdent = new IdentToken(buffString);
					newIdent.setValue(0);
					addIdentifier(newIdent);
					
					return newIdent;
				}
			}
		}
		/* If it gets to this stage, the user has input a value which does
	 	not exist in Euclid */
		System.out.println("-----------");
		System.out.println("Error in lexical analysis of program.");
		System.out.println("Line number " + lineNumber + " has caused this error!");
		System.out.println("-----------");
		return new Token(TokenType.NULL_TOKEN);
	}

	private Token checkIfComment() throws IOException {
		StringBuffer commentBuffer = new StringBuffer();
		do {
			commentBuffer.append(peek);
			peek = (char) System.in.read();
		} while(peek != '\t' && peek != '\n' && peek != '\r' && commentBuffer.toString() != "/*");
		peek = ' ';
		
		String commentBufferString = commentBuffer.toString();
		
		if(commentBufferString.indexOf("*/") >= 0) {
			return new Token(TokenType.COMMENT);
		}
		// Checks to see if there are two // to confirm it is a comment
		if(commentBufferString.indexOf("//") >= 0) {
			return new Token(TokenType.COMMENT);
		}
		
		// If comment is /* then remove all characters until end of */
		if(commentBufferString.indexOf("/*") >= 0 && commentBufferString.indexOf("*/") < 0){
			do {
				commentBuffer.append(peek);
				peek = (char) System.in.read();
			} while(commentBuffer.toString().indexOf("*/") < 0);
			return new Token(TokenType.COMMENT);
		}
		return null;
	}

	public Hashtable<String, IdentToken> getIdentifiers() {
		return identifiers;
	}
	
	// Checks to see if ID is uppercase - displays informative error message
	public static boolean isIdUpperCase(String id) {
		boolean isUppercase = false;
		// Loop through each character, if uppercase then return true
	    for (int i=0; i<id.length(); i++){
	        if (Character.isUpperCase(id.charAt(i))) {
	        	isUppercase = true;
	        }
	    }
	    return isUppercase;
	}
}
