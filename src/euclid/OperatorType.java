
package euclid;

public enum OperatorType {
    PLUS, MINUS, MULTIPLY, MODULUS, GREATER_THAN, LESS_THAN, NOT_EQUAL
}
