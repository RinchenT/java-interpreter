
package euclid;

import java.util.Hashtable;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

public class Parser {
	public Token[] tokens; // Array of tokens to be parsed
	public int position; // Current position in array

	Hashtable<String, Integer> vars; // Hashtable of all identifiers
	
	public Parser(Token[] tokenSeq, Hashtable variables) {
		tokens = tokenSeq;
		position = 0; // Current position in sequence
		vars = variables;
	}

	private void parseError(String string) {
		System.out.println("Parse error: expected " + string);
	}

	private void match(TokenType tokType) {
		if (tokens[position].returnType() != tokType) {
			parseError(tokType.toString());
		}
		position++;
	}

	private boolean check(TokenType tokType) {
		if (tokens[position].returnType() != tokType) {
			return false;
		}
		return true;
	}
	
	private boolean keywordCheck(String keyword) {
		boolean isKeyword = false;
		// Checks to see if current position is a keyword
		if(tokens[position].returnType() == TokenType.KEYWORD) {
			
			// If it's a keyword, it checks to see if it the keyword system is looking for
			if(((KeywordToken)tokens[position]).getKeyword() == keyword) {
				isKeyword = true;
			}
		}
		return isKeyword;
	}

	// Start to parse the program
	public void prog() {
		try {
			// First parse declarations
			decl();
			// Next parse expressions
			exprs();
			// Finally parse the end of file character
			end_of_file();
		}catch(Exception e) {
			// Error handler
		}
	}
	
	private void decl() {
		switch (tokens[position].returnType()) {
		// This checks to see if it is in the format of KEYWORD ID ;
		case KEYWORD:
			if (keywordCheck("INT")) {
				match(TokenType.KEYWORD);
				if (tokens[position].returnType() == TokenType.ID) {
					match(TokenType.ID);
					if (tokens[position].returnType() == TokenType.END_OF_LINE) {
						match(TokenType.END_OF_LINE);
						decl();
					} else {
						parseError("end of line ';'");
					}
				} else {
					parseError("a valid ID name, starting with a letter");
				}
			}
			break;
		default:
			break;
		}
	}

	private void exprs() {
		statement();
	}

	@SuppressWarnings("unlikely-arg-type")
	private void statement() {
		// Values used to temporary hold data which will be used for calculation
		IdentToken id1 = null; 
		IdentToken id2 = null;
		
		switch (tokens[position].returnType()) {

		case ID:
			id1 = ((IdentToken) tokens[position]);
			match(TokenType.ID);
			if (check(TokenType.EQUALS)) {
				match(TokenType.EQUALS);

				// If below is true: ID IS NUM;
				if (check(TokenType.NUM)) {
					NumToken num1 = ((NumToken) tokens[position]); // x is 5 + 5;
					match(TokenType.NUM);
					if (check(TokenType.END_OF_LINE)) {
						vars.put(id1.getIdName(), ((NumToken) tokens[position - 1]).getValue()); // Eg. y is 10;
						end_of_line();
					} else {
						// If below is true: ID IS NUM OPERATOR...
						if (check(TokenType.OPERATOR)) {
							OperatorType op1 = ((OperatorToken) tokens[position]).getType(); 
							match(TokenType.OPERATOR);
							// If below is true: ID IS NUM OPERATOR ID / NUM
							if (check(TokenType.ID)) {
								IDISNUMCalculateID(id1, num1, op1, ((IdentToken) tokens[position]));
								match(TokenType.ID);
								end_of_line();
							} else if (check(TokenType.NUM)) {
								IDISNUMCalculateNUM(id1, num1, op1, ((NumToken) tokens[position]));
								match(TokenType.NUM);
								end_of_line();
							} else {
								parseError("ID or Number"); // Informative error messages displaying errors on each section
							}
						} else {
							parseError("a valid operator");
						}
					}

				} else if (check(TokenType.ID)) {
					id2 = ((IdentToken) tokens[position]);
					vars.put(id1.getIdName(), vars.get(id2.getIdName()));

					match(TokenType.ID);
					// If below is true: ID IS ID;
					if (check(TokenType.END_OF_LINE)) {
						match(TokenType.END_OF_LINE);
						statement();

						// If below is true: ID IS ID OPERATOR ID / NUM;
					} else if (check(TokenType.OPERATOR)) {
						OperatorType op1 = ((OperatorToken) tokens[position]).getType();
						match(TokenType.OPERATOR);
						if (check(TokenType.ID)) {
							IDISIDCalculateID(id1, id2, op1, ((IdentToken) tokens[position]));
							match(TokenType.ID);
							end_of_line();
						} else if (check(TokenType.NUM)) {
							IDISIDCalculateNUM(id1, id2, op1, ((NumToken) tokens[position]));
							match(TokenType.NUM);
							end_of_line();
						} else {
							parseError("ID or Number");
						}
					} else {
						parseError("Valid operator or End of Line");
					}
				}
			}
			break;
		case KEYWORD:
			IdentToken compareVal;
			OperatorToken operator;
			NumToken number;
			// Checking to see if OUTPUT format is: OUTPUT LBRACKET ID RIGHTBRACKET
			// END_OF_LINE
			if (keywordCheck("OUTPUT")) {
				match(TokenType.KEYWORD);
				if (check(TokenType.LBRACKET)) {
					match(TokenType.LBRACKET);
					if (check(TokenType.ID)) {
						int tempPositionHolder = position;
						match(TokenType.ID);
						if (check(TokenType.RBRACKET)) {
							match(TokenType.RBRACKET);
							if (check(TokenType.END_OF_LINE)) {
								match(TokenType.END_OF_LINE);
								// CHecks the hashtable for key and prints out value.
								if(vars.get(((IdentToken)tokens[tempPositionHolder]).getIdName()) == null){
									System.out.println("ERROR on OUTPUT: " + (((IdentToken)tokens[tempPositionHolder]).getIdName() + " has not been instantiated!"));
								} else {
									System.out.println(vars.get(((IdentToken) tokens[tempPositionHolder]).getIdName()));
								}
								exprs();
							}
						}
					}
				}
			} else if (keywordCheck("WHILE")) {
				// Format of WHILE: KEYWORD LBRACKET ID OPERATOR NUM RBRACKET THEN
				// TODO: make it so it checks THEN, ENDWHILE instead of just keyword

				match(TokenType.KEYWORD);
				if (check(TokenType.LBRACKET)) {
					match(TokenType.LBRACKET);
					if (check(TokenType.ID)) {
						compareVal = ((IdentToken) tokens[position]);
						match(TokenType.ID);
						if (check(TokenType.OPERATOR)) {
							operator = ((OperatorToken) tokens[position]);
							match(TokenType.OPERATOR);
							if (check(TokenType.NUM)) {
								number = ((NumToken) tokens[position]);
								match(TokenType.NUM);
								if (check(TokenType.RBRACKET)) {
									match(TokenType.RBRACKET);
									if (check(TokenType.KEYWORD)) {
										match(TokenType.KEYWORD);
										int tempPosition = position;
										boolean endLoop = false;
										while (!endLoop) {
											if (endWhileLoop(compareVal, operator, number)) {
												// IF while loop is not correct
												endLoop = true;

												if(check(TokenType.KEYWORD)) {
													match(TokenType.KEYWORD);
													while(!keywordCheck("ENDWHILE")) {
														position++;
													}
													if(check(TokenType.END_OF_FILE)) {
														end_of_file();
													} else {
														position++;
														exprs();
														end_of_file();
													}
												}
											} else {
												// If WHILE loop condition is correct
												position = tempPosition;
												exprs();
												// Checks to see if ENDWHILE is excuted
												if (keywordCheck("ENDWHILE")) {
													match(TokenType.KEYWORD);
													// Checks to see if it is END OF FILE or if user
													// has entered more expressions
													if(check(TokenType.END_OF_FILE)) {
														end_of_file();
													} else {
														exprs();
														position++;
													}
												} else {
													// If no endwhile, check to see if END OF FILE
													if(check(TokenType.END_OF_FILE)) {
														end_of_file();
													} else {
														// If no end of file, get user input
														position++;
														exprs();
														
														System.out.println(tokens[position].returnType());
														if(check(TokenType.END_OF_FILE)) {
															end_of_file();
															endLoop = true;
														}
													}
												}
											}
										}
										position--;
									}
								}
							}
						}
					}
				}
			} else if (keywordCheck("IF")) {
				match(TokenType.KEYWORD);
				if (check(TokenType.LBRACKET)) {
					match(TokenType.LBRACKET);
					if (check(TokenType.ID)) {
						compareVal = ((IdentToken) tokens[position]);
						match(TokenType.ID);
						if (check(TokenType.OPERATOR)) {
							operator = ((OperatorToken) tokens[position]);
							match(TokenType.OPERATOR);
							if (check(TokenType.NUM)) {
								number = ((NumToken) tokens[position]);
								match(TokenType.NUM);
								if (check(TokenType.RBRACKET)) {
									match(TokenType.RBRACKET);
									if (check(TokenType.KEYWORD)) {
										match(TokenType.KEYWORD);
										if (checkIfTrue(compareVal, operator, number)) {
											exprs();
										} else {
											while(!keywordCheck("ENDIF")) {
												position++;
											}											
										}
										if(check(TokenType.END_OF_FILE)) {
											end_of_file();
										} else {
											position++;
											exprs();
											end_of_file();
										}
										position--;
										exprs();
										if (check(TokenType.KEYWORD)) {
											match(TokenType.KEYWORD);
										}
									
									}
								}
							}
						}
					}
				}
			}
			break;
		default:
			break;

		}

	}

	private boolean endWhileLoop(IdentToken compareVal, OperatorToken operator, NumToken number) {
		boolean endLoop = false;
		
		// Compares values depending on operator type
		switch (operator.getType()) {
		case LESS_THAN:
			if (vars.get(compareVal.getIdName()) >= number.getValue()) {
				endLoop = true;
			}
			break;
		case GREATER_THAN:
			if (vars.get(compareVal.getIdName()) <= number.getValue()) {
				endLoop = true;
			}
		case NOT_EQUAL:
			if (vars.get(compareVal.getIdName()) == number.getValue()) {
				endLoop = true;
			}
			break;
		}
		return endLoop;
	}

	private boolean checkIfTrue(IdentToken compareVal, OperatorToken operator, NumToken number) {
		boolean isTrue = false;

		switch (operator.getType()) {
		case LESS_THAN:
			if (vars.get(compareVal.getIdName()) < number.getValue()) {
				isTrue = true;
			}
			break;
		case GREATER_THAN:
			if (vars.get(compareVal.getIdName()) > number.getValue()) {
				isTrue = true;
			}
			break;
		case NOT_EQUAL:
			if (vars.get(compareVal.getIdName()) != number.getValue()) {
				isTrue = true;
			}
			break;
		}
		return isTrue;
	}

	private void end_of_file() {
		if (check(TokenType.END_OF_FILE)) {
			match(TokenType.END_OF_FILE);
		} else {
			parseError("End of File symbol '$' after end of line or loop");
		}
	}

	private void end_of_line() {
		if (check(TokenType.END_OF_LINE)) {
			match(TokenType.END_OF_LINE);
			statement();
		} else {
			parseError("End of Line declaration ';' ");
		}
	}

	private void IDISIDCalculateID(IdentToken id1, IdentToken id2, OperatorType op1, IdentToken valToCalculate) {
		int result = 0;
		int id2Value = vars.get((((IdentToken) id2).getIdName()));
		int valueToCalc = vars.get(((IdentToken) valToCalculate).getIdName());

		switch (op1) {
		case PLUS:
			result = id2Value + valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MINUS:
			result = id2Value - valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MULTIPLY:
			result = id2Value * valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MODULUS:
			result = id2Value % valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		}
	}

	private void IDISIDCalculateNUM(IdentToken id1, IdentToken id2, OperatorType op1, NumToken valToCalculate) {
		int result = 0;
		int id2Value = vars.get((((IdentToken) id2).getIdName()));
		int valueToCalc = ((NumToken) valToCalculate).getValue();

		switch (op1) {
		case PLUS:
			result = id2Value + valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MINUS:
			result = id2Value - valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MULTIPLY:
			result = id2Value * valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MODULUS:
			result = id2Value % valueToCalc;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		}
	}

	private void IDISNUMCalculateID(IdentToken id1, NumToken num1, OperatorType op1, IdentToken tokens2) {
		int result = 0;
		int tokenValue = vars.get(tokens2.getIdName());
		switch (op1) {
		case PLUS:
			result = num1.getValue() + tokenValue;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MINUS:
			result = num1.getValue() - tokenValue;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MULTIPLY:
			result = num1.getValue() * tokenValue;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MODULUS:
			result = num1.getValue() % tokenValue;
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		}

	}

	private void IDISNUMCalculateNUM(IdentToken id1, NumToken num1, OperatorType op1, NumToken num2) {

		int result = 0;

		switch (op1) {
		case PLUS:
			result = num1.getValue() + num2.getValue();
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MINUS:
			result = num1.getValue() - num2.getValue();
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MULTIPLY:
			result = num1.getValue() * num2.getValue();
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		case MODULUS:
			result = num1.getValue() % num2.getValue();
			vars.put(id1.getIdName(), result);
			id1.setValue(result);
			break;
		}
	}
}